import React, {useEffect, useState}from 'react';
import { StyleSheet, Text, View, ImageBackground, Button, Image } from 'react-native';
import { NavigationContainer, useNavigation } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import CocktailScroll from './components/CocktailScroll'
import CocktailDetails from './components/CocktailDetails'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import {
    faHeart,
    faUser,
    faMartiniGlassCitrus,
  } from "@fortawesome/free-solid-svg-icons";
import Pressable from 'react-native/Libraries/Components/Pressable/Pressable';
import FavCocktails from './components/FavCocktails';
import Profile from './components/Profile';

const imgBackground = require('./assets/bg.jpg')
const Stack = createNativeStackNavigator();
export default function App() {
  const [items, setItems] = useState([]);
  const [data, setData] = useState(null);
  useEffect(() => 
  {
    (async () => 
    {
      fetchDataFromApi();
    })();
  }, [])
  const fetchDataFromApi = () => {
     fetch(`https://www.thecocktaildb.com/api/json/v1/1/search.php?f=a`)
     .then(response => response.json())
     .then(data => {
       setData(data.drinks)
      })
     .catch((error) => {
       console.error(error)
     })
     //.finally(() => setData(false)); 
  }
  const removeItem =(value) =>
  {
    items.splice(items.indexOf(value), 1);
    setItems([...items]);
  }
  const addItem = (value) =>
  {
    //check if value exist in array
    if(items.indexOf(value) > -1 !=true)
    {
      setItems([...items, value]);
    }
    else
    {
      items.splice(items.indexOf(value), 1);
      setItems([...items]);
    }
    console.log('items in array', items)
  }
  function LogoTitle() {
    return (
      <Image
        style={{ width: 130, height: 25 }}
        source={require('./assets/shakeit.png')}
      />
    );
  }
  function HomeScreen({ navigation }) {
    return (
      <View style={styles.container}>
        <ImageBackground source={imgBackground} style={styles.imageBackground} >
          <CocktailScroll data={data} items={items} addItem={addItem}/>
          <BottomMenu></BottomMenu>
        </ImageBackground>
      </View>
    );
  }
  function DetailsScreen({ route, navigation }) {
    const { itemId } = route.params;
    return (
      <View style={styles.container}>
        <ImageBackground source={imgBackground} style={styles.imageBackground} >
          <CocktailDetails itemId={itemId} items={items} addItem={addItem}/>
          <BottomMenu></BottomMenu>
        </ImageBackground>
      </View>
    );
  }
  function FavScreen({ route, navigation }) {
    const { itemId } = route.params;
    return (
      <View style={styles.container}>
        <ImageBackground source={imgBackground} style={styles.imageBackground} >
          <FavCocktails data={data} items={items} addItem={addItem}/>
          <BottomMenu></BottomMenu>
        </ImageBackground>
      </View>
    );
  }
  function ProfileScreen({ route, navigation }) {
    return (
      <View style={styles.container}>
        <ImageBackground source={imgBackground} style={styles.imageBackground} >
          <Profile />
          <BottomMenu></BottomMenu>
        </ImageBackground>
      </View>
    );
  }
  const BottomMenu = () =>
    {
      const navigation = useNavigation();
      return (
      <View style={styles.bottomMenu}>
                <Pressable onPress={() => navigation.navigate('Home')} style={styles.button}>
                  <FontAwesomeIcon icon={faMartiniGlassCitrus} size={25}/>
                </Pressable>

                <Pressable onPress={() => navigation.navigate('Favorite')} style={styles.button}>
                  <FontAwesomeIcon icon={faHeart} size={25}/>
                </Pressable>
                
                <Pressable onPress={() => navigation.navigate('Profile')} style={styles.button}>
                  <FontAwesomeIcon icon={faUser} size={25}/>
                </Pressable>
            </View>
      );
    }
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home" screenOptions={{
        headerStyle: {
          backgroundColor: '#00B9CD',
        },
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }}>
        <Stack.Screen name="Home" component={HomeScreen} options={{ headerTitle: (props) => <LogoTitle {...props}  /> }}/>
        <Stack.Screen name="Details" component={DetailsScreen} options={{title:'Details'}} initialParams = {data} />
        <Stack.Screen name="Favorite" component={FavScreen} options={{title:'favorites'}} initialParams = {data} />
        <Stack.Screen name="Profile" component={ProfileScreen} options={{title:'Profile'}}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imageBackground:{
    flex:1, 
    resizeMode:"cover", 
    justifyContent:"center"
  },
  bottomMenu: {
    flexDirection:'row',
    backgroundColor:"#00B9CD",
    justifyContent:"space-between",
    padding: 5,
    paddingLeft: 40,
    paddingRight:40,
    borderTopColor:"#029aaa",
    borderTopWidth: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  }
  
});
