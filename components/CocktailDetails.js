import React, {useEffect, useState}from 'react';
import {View, ScrollView, Image, Text, StyleSheet, Dimensions, Button, TouchableHighlight, Pressable} from 'react-native'
import { useNavigation } from '@react-navigation/native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import {
    faHeart,
    faMartiniGlass,
    faLayerGroup,
    faWineGlass,
    faArrowLeft,
  } from "@fortawesome/free-solid-svg-icons";


const CocktailDetails = (props) => {
    const [data, setData] = useState({});
    const navigation = useNavigation();
    useEffect(() => 
    {
    (async () => 
    {
        fetchDataFromApi();
    })();
    }, [])
    const fetchDataFromApi = () => {
        fetch(`https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${props.itemId}`)
        .then(response => response.json())
        .then(data => {
        setData(data.drinks[0])
        })
        .catch((error) => {
        console.error(error)
        })
        //.finally(() => setData(false)); 
    }
    const CocktailItem = ({CocktailItem}) => 
    {
        const img = {uri: CocktailItem.strDrinkThumb}
        console.log('img',CocktailItem)
        
        return (
            <View  style={styles.item}>
                
                    <Image source={img} style={styles.image} />
                <View style={{flexDirection:'row', width:'100%', justifyContent:'center'}}>
                    
                    <View style={{flexDirection:'column', padding:6, textAlign:'center'}}>
                        <Text  style={styles.title}><FontAwesomeIcon icon={faMartiniGlass}/> {CocktailItem.strDrink}</Text>
                    </View>
                    <View style={{flexDirection:'column', padding:6, textAlign:'center'}}><Text  style={styles.title}><FontAwesomeIcon icon={faLayerGroup}/> {CocktailItem.strCategory}</Text></View>
                    <View style={{flexDirection:'column', padding:6, textAlign:'center'}}>
                        <Text  style={styles.title}><FontAwesomeIcon icon={faWineGlass}/>  {CocktailItem.strAlcoholic}</Text>
                    </View>
                </View>
                {
                    props.items && props.items.find(e => e == CocktailItem.idDrink)?
                    <Pressable style={styles.button} onPress={() => props.addItem(CocktailItem.idDrink)}>
                        <FontAwesomeIcon icon={faHeart} size={25}  style={{color:'purple'}}/>
                    </Pressable>:
                    <Pressable style={styles.button} onPress={() => props.addItem(CocktailItem.idDrink)}>
                        <FontAwesomeIcon icon={faHeart} size={25}  />
                    </Pressable>
                }
                <View style={{flexDirection:'row', flexWrap: 'wrap', width:'100%', justifyContent:'center', marginTop:10}}>
                    <Text  style={styles.title}><Text style={{fontWeight:'400'}}>Ingredients </Text>: </Text>
                    {
                            CocktailItem.strIngredient1?
                            <Text  style={styles.text}>{CocktailItem.strIngredient1}, </Text>
                            :
                            <Text/>
                    }
                    {
                            CocktailItem.strIngredient2?
                            <Text  style={styles.text}>{CocktailItem.strIngredient2}, </Text>
                            :
                            <Text/>
                    }
                    {
                            CocktailItem.strIngredient3?
                            <Text  style={styles.text}>{CocktailItem.strIngredient3}, </Text>
                            :
                            <Text/>
                    }
                    {
                            CocktailItem.strIngredient4?
                            <Text  style={styles.text}>{CocktailItem.strIngredient4}, </Text>
                            :
                            <Text/>
                    }
                    {
                            CocktailItem.strIngredient5?
                            <Text  style={styles.text}>{CocktailItem.strIngredient5}, </Text>
                            :
                            <Text/>
                    }
                    {
                            CocktailItem.strIngredient6?
                            <Text  style={styles.text}>{CocktailItem.strIngredient6}, </Text>
                            :
                            <Text/>
                    }
                    {
                            CocktailItem.strIngredient7?
                            <Text  style={styles.text}>{CocktailItem.strIngredient7}, </Text>
                            :
                            <Text/>
                    }
                    {
                            CocktailItem.strIngredient8?
                            <Text  style={styles.text}>{CocktailItem.strIngredient8}, </Text>
                            :
                            <Text/>
                    }
                    {
                            CocktailItem.strIngredient9?
                            <Text  style={styles.text}>{CocktailItem.strIngredient9}, </Text>
                            :
                            <Text/>
                    }
                    {
                            CocktailItem.strIngredient10?
                            <Text  style={styles.text}>{CocktailItem.strIngredient10}, </Text>
                            :
                            <Text/>
                    }
                    {
                            CocktailItem.strIngredient11?
                            <Text  style={styles.text}>{CocktailItem.strIngredient11}, </Text>
                            :
                            <Text/>
                    }
                    {
                            CocktailItem.strIngredient12?
                            <Text  style={styles.text}>{CocktailItem.strIngredient12}, </Text>
                            :
                            <Text/>
                    }
                    {
                            CocktailItem.strIngredient13?
                            <Text  style={styles.text}>{CocktailItem.strIngredient13}, </Text>
                            :
                            <Text/>
                    }
                    {
                            CocktailItem.strIngredient14?
                            <Text  style={styles.text}>{CocktailItem.strIngredient14}, </Text>
                            :
                            <Text/>
                    }
                    {
                            CocktailItem.strIngredient15?
                            <Text  style={styles.text}>{CocktailItem.strIngredient15}, </Text>
                            :
                            <Text/>
                    }
                </View>
                <View style={{flexDirection:'row', flexWrap: 'wrap', width:'100%', justifyContent:'center', marginTop:10}}>
                    
                        <Text  style={styles.text}><Text style={{fontWeight:'400'}}>Instructions : </Text>{CocktailItem.strInstructions}</Text>
                    
                </View>
                
            </View>
        )
    }
    return (
        <ScrollView vertical={true} style={styles.scrollView}>
            <View  style={styles.ItemContainer}>
                <CocktailItem CocktailItem= {data}></CocktailItem>
            </View>
            <View style={styles.BackBottom} horizontal={false}>
                <Pressable onPress={() => navigation.goBack()} style={styles.button}>
                <Text  style={styles.title}><FontAwesomeIcon icon={faArrowLeft} size={25}/></Text>
                </Pressable>
            </View>
        </ScrollView>
    )
}
// Get device width
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    ItemContainer: {
        flexWrap: 'wrap',
        flex:1,
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 20,
        paddingTop: 0,
        marginBottom: 20,
        paddingBottom: 20,
        backgroundColor: "#00B9CD",
        borderRadius: 10,
    },
    item: {
        height: deviceHeight -150,
        width: deviceWidth * 1,
        backgroundColor: "#00B9CD",
        borderRadius: 10,
        margin:4,
        marginTop:0
    },
    scrollView: {
        flexDirection:'column',
        backgroundColor: 'rgba(0, 0, 0, 0.4)',
    },
    image: {
        flex: 1,
        borderTopLeftRadius:10,
        borderTopRightRadius:10,
        height: undefined,
        width: undefined
    }, 
    title: {
        fontSize: 14,
        textAlign:"center",
        fontWeight: "400",
    },   
    text: {
        fontSize: 14,
        textAlign:"center",
        fontWeight: "300",
    },   
    button: {
        fontSize: 14,
        fontWeight:"200",
        textAlign:"center",
        alignSelf: 'center',
        margin: 5,
        backgroundColor: "#00B9CD"
    },
    BackBottom: {
        flexDirection:'row',
        backgroundColor:"#00B9CD",
        justifyContent:"space-between",
        padding:4,
        paddingLeft: 20,
        paddingRight: 20,
        marginBottom: 20,
        marginTop:0,
        borderRadius:10,
        borderTopColor:"#029aaa",
        borderTopWidth: 1,
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
      }
})

export default CocktailDetails
