import React from 'react'
import {View, ScrollView, Image, Text, StyleSheet, Dimensions, Button, TouchableHighlight, Pressable, TextInput} from 'react-native'
import { useNavigation } from '@react-navigation/native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import {
    faArrowLeft,
    faHeart,
  } from "@fortawesome/free-solid-svg-icons";
const Profile = () => {
    const navigation = useNavigation();
    return (
        <ScrollView vertical={true} style={styles.scrollView}>
            
            <View style={styles.ItemContainer} horizontal={false}>
                <View  style={styles.item} >
                    
                    <Image source={require('../assets/profile.jpg')} style={styles.image} />
                    <View style={{flexDirection:'row', flexWrap: 'wrap', width:'100%', justifyContent:'center', marginTop:10}}>
                    
                            <Text  style={styles.text}> This Simple open source application is written by Omid Amini in 2022 as a school project with the help of Professor Aurélien Chiren at ORT Lyon. </Text>
                        
                    </View>
                    <View style={{flexDirection:'row', flexWrap: 'wrap', width:'100%', justifyContent:'center', marginTop:10}}>
                    
                            <Text  style={styles.text}> All recipes and instructions are from The CocktailDB, an open source API, the author of this app takes no responsibility.  </Text>
                        
                    </View>
                    <View style={{flexDirection:'row', flexWrap: 'wrap', width:'100%', justifyContent:'center', marginTop:10}}>
                    
                            <Text  style={styles.text}> if you see a problem on this application, please report it to this email: omidamini@ymail.com. to download the source code of this application go to: https://gitlab.com/omidamini/recipes-de-cocktail  </Text>
                        
                    </View>
                </View>
            </View>
            <View style={styles.BackBottom} horizontal={false}>
                <Pressable onPress={() => navigation.goBack()} style={styles.button}>
                <Text  style={styles.title}><FontAwesomeIcon icon={faArrowLeft} size={25}/></Text>
                </Pressable>
            </View>
        </ScrollView>
    )
}
// Get device width
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
    ItemContainer: {
        flexWrap: 'wrap',
        flex:1,
        flexDirection: 'row',
        justifyContent: 'center',
        margin: 10,
        paddingTop: 0,
        marginBottom: 20,
        paddingBottom: 20,
        backgroundColor: "#00B9CD",
        borderRadius: 10,
    },
    item: {
        height: deviceHeight -230,
        
        borderRadius: 10,
        margin:20,
        marginTop:0
    },
    scrollView: {
        flexDirection:'column',
        backgroundColor: 'rgba(0, 0, 0, 0.4)',
    },
    image: {
        margin:10,
        marginTop:30,
        alignSelf:'center',
        borderRadius:50,
        height: 100,
        width: 100
    }, 
    title: {
        fontSize: 14,
        textAlign:"center",
        fontWeight: "400",
    },   
    text: {
        fontSize: 14,
        textAlign:"center",
        fontWeight: "300",
    },   
    button: {
        fontSize: 14,
        fontWeight:"200",
        textAlign:"center",
        alignSelf: 'center',
        margin: 5,
    },
    BackBottom: {
        flexDirection:'row',
        backgroundColor:"#00B9CD",
        justifyContent:"space-between",
        padding:4,
        paddingLeft: 20,
        paddingRight: 20,
        marginBottom: 20,
        marginTop:0,
        margin:10,
        borderRadius:10,
        borderTopColor:"#029aaa",
        borderTopWidth: 1,
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
      }
})

export default Profile
