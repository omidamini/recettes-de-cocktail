import React from 'react'
import {View, ScrollView,FlatList, Image, Text, StyleSheet, Dimensions, Button, TouchableHighlight, Pressable, TextInput} from 'react-native'
import { useNavigation } from '@react-navigation/native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import {
    faHeart,
  } from "@fortawesome/free-solid-svg-icons";
const CocktailScroll = (props) => {
    const CocktailListItem = ({CocktailItem}) => {
        const img = {uri: CocktailItem.strDrinkThumb}
        const navigation = useNavigation();
    
        return (
            <View  style={styles.item} >
                <TouchableHighlight style={styles.sub_item}  onPress={() => navigation.navigate('Details', {itemId: CocktailItem.idDrink,})}>
                    <Image source={img} style={styles.image} />
                </TouchableHighlight>
                <Text  style={styles.title}>{CocktailItem.strDrink}</Text>
                {
                    props.items && props.items.find(e => e == CocktailItem.idDrink)?
                    <Pressable style={styles.button} onPress={() => props.addItem(CocktailItem.idDrink)}>
                        <FontAwesomeIcon icon={faHeart} size={25}  style={{color:'purple'}}/>
                    </Pressable>:
                    <Pressable style={styles.button} onPress={() => props.addItem(CocktailItem.idDrink)}>
                        <FontAwesomeIcon icon={faHeart} size={25}  />
                    </Pressable>
                }
                
            </View>
        )
    }
    
    var data = props.data
    return (
        
        <ScrollView vertical={true} style={styles.scrollView}>
            <View  style={styles.ItemContainer} horizontal={false}>
                {
                    data && data.length > 0 ? 

                    data.map((data, idx) => (

                        idx !== 0 &&  <CocktailListItem key={idx} CocktailItem={data}/>
                    ))

                    :
                    <View/>
                }
            </View>
        </ScrollView>
    )
}
// Get device width
const deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
    ItemContainer: {
        flexWrap: 'wrap',
        flex:1,
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 20,
        paddingBottom: 20
    },
    item: {
        height: deviceWidth * 0.6,
        width: deviceWidth * 0.4,
        backgroundColor: "#00B9CD",
        borderRadius: 10,
        margin:4
    },
    sub_item: {
        height: "75%",
        width: "100%",
        backgroundColor:'#00B9CD',
        borderRadius: 10,
    },
    scrollView: {
        flexDirection:'column',
        backgroundColor: 'rgba(0, 0, 0, 0.4)',
    },
    image: {
        flex: 1,
        borderTopLeftRadius:10,
        borderTopRightRadius:10,
        height: undefined,
        width: undefined
    }, 
    title: {
        fontSize: 14,
        textAlign:"center",
        fontWeight: "400",
    },   
    text: {
        fontSize: 14,
        textAlign:"center",
        fontWeight: "300",
    },  
    button: {
        fontSize: 14,
        fontWeight:"200",
        textAlign:"center",
        alignSelf: 'center',
        margin: 5,
        backgroundColor: "#00B9CD"
    },
    buttonRed: {
        fontSize: 14,
        fontWeight:"200",
        textAlign:"center",
        alignSelf: 'center',
        margin: 5,
        backgroundColor: "red"
    }
})

export default CocktailScroll
