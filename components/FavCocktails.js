import React from 'react'
import {View, ScrollView, Image, Text, StyleSheet, Dimensions, Button, TouchableHighlight, Pressable, TextInput} from 'react-native'
import { useNavigation } from '@react-navigation/native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import {
    faArrowLeft,
    faHeart,
  } from "@fortawesome/free-solid-svg-icons";
const FavCocktails = (props) => {
    const CocktailListItem = ({CocktailItem}) => {
        const img = {uri: CocktailItem.strDrinkThumb}
        const navigation = useNavigation();
        return (
            props.items && props.items.find(e => e == CocktailItem.idDrink)?
            <View  style={styles.item} >
                <TouchableHighlight style={styles.sub_item}  onPress={() => navigation.navigate('Details', {itemId: CocktailItem.idDrink,})}>
                    <Image source={img} style={styles.image} />
                </TouchableHighlight>
                <Text  style={styles.title}>{CocktailItem.strDrink}</Text>
                {
                    props.items && props.items.find(e => e == CocktailItem.idDrink)?
                    <Pressable style={styles.button} onPress={() => props.addItem(CocktailItem.idDrink)}>
                        <FontAwesomeIcon icon={faHeart} size={25}  style={{color:'purple'}}/>
                    </Pressable>:
                    <Pressable style={styles.button} onPress={() => props.addItem(CocktailItem.idDrink)}>
                        <FontAwesomeIcon icon={faHeart} size={25}  />
                    </Pressable>
                }
            </View>:<View/>
        )
    }
    var data = props.data;
    console.log('fav',data);
    const navigation = useNavigation();
    return (
        <ScrollView vertical={true} style={styles.scrollView}>
            <View style={styles.ItemContainer} horizontal={false}>
                {
                    props.items && props.items.length>0 && data && data.length > 0 ? 

                    data.map((data, idx) => (

                        idx !== 0 &&  <CocktailListItem key={idx} CocktailItem={data}/>
                    ))
                    :
                    <View  style={styles.CocktailListItemContainer} >
                        <Text  style={styles.title}>Your favorites list is empty.</Text>
                    </View>
                }
            </View>
            <View style={styles.BackBottom} horizontal={false}>
                <Pressable onPress={() => navigation.goBack()} style={styles.button}>
                <Text  style={styles.title}><FontAwesomeIcon icon={faArrowLeft} size={25}/></Text>
                </Pressable>
            </View>
        </ScrollView>
    )
}
// Get device width
const deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
    ItemContainer: {
        flexWrap: 'wrap',
        flex:1,
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 20,
        paddingBottom: 20,
    },
    item: {
        height: deviceWidth * 0.6,
        width: deviceWidth * 0.4,
        backgroundColor: "#00B9CD",
        borderRadius: 10,
        margin:4
    },
    sub_item: {
        height: "75%",
        width: "100%",
        backgroundColor:'#00B9CD',
        borderRadius: 10,
    },
    scrollView: {
        flexDirection:'column',
        backgroundColor: 'rgba(0, 0, 0, 0.4)',
    },
    image: {
        flex: 1,
        borderTopLeftRadius:10,
        borderTopRightRadius:10,
        height: undefined,
        width: undefined
    }, 
    CocktailListItemContainer: {
        flex:1,
        justifyContent: 'center',
        backgroundColor: '#00B9CD',
        borderRadius:10,
        padding: 20,
        margin: 10,
        marginBottom:1,
    }, 
    title: {
        fontSize: 14,
        textAlign:"center",
        fontWeight: "400",
    },   
    text: {
        fontSize: 14,
        textAlign:"center",
        fontWeight: "300",
    },  
    button: {
        fontSize: 14,
        fontWeight:"200",
        textAlign:"center",
        alignSelf: 'center',
        margin: 5,
        backgroundColor: "#00B9CD"
    },
    buttonRed: {
        fontSize: 14,
        fontWeight:"200",
        textAlign:"center",
        alignSelf: 'center',
        margin: 5,
        backgroundColor: "red"
    },
    BackBottom: {
        flexDirection:'row',
        backgroundColor:"#00B9CD",
        justifyContent:"space-between",
        padding:4,
        paddingLeft: 20,
        paddingRight: 20,
        margin: 10,
        marginTop:0,
        borderRadius:10,
        borderTopColor:"#029aaa",
        borderTopWidth: 1,
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
      }
})

export default FavCocktails
